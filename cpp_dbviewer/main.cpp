//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "main.h"
#include "About.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TDBViewerForm *DBViewerForm;
//---------------------------------------------------------------------------
__fastcall TDBViewerForm::TDBViewerForm(TComponent* Owner)
   : TForm(Owner)
{
}
//---------------------------------------------------------------------------



void __fastcall TDBViewerForm::FormCreate(TObject *Sender){
   opendb();
   }
//---------------------------------------------------------------------------

void __fastcall TDBViewerForm::NVihodClick(TObject *Sender){
   Close();
   }
//---------------------------------------------------------------------------

void __fastcall TDBViewerForm::NOtkritClick(TObject *Sender){
   opendb();
   }
//---------------------------------------------------------------------------

void __fastcall TDBViewerForm::NAboutClick(TObject *Sender){
   AboutBox->ShowModal();
   }
//---------------------------------------------------------------------------

