object DBViewerForm: TDBViewerForm
  Left = 228
  Top = 145
  Width = 593
  Height = 623
  Cursor = crIBeam
  Anchors = []
  AutoSize = True
  BorderIcons = [biSystemMenu]
  Caption = 'DBViewer'
  Color = clBtnFace
  DockSite = True
  ParentFont = True
  Menu = MainMenu
  OldCreateOrder = False
  Position = poDefault
  ShowHint = True
  Visible = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid: TDBGrid
    Left = 0
    Top = 0
    Width = 585
    Height = 545
    DataSource = DataSource
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBNavigator1: TDBNavigator
    Left = 0
    Top = 552
    Width = 580
    Height = 25
    DataSource = DataSource
    TabOrder = 1
  end
  object MainMenu: TMainMenu
    Left = 64
    Top = 40
    object A1: TMenuItem
      Caption = #1060#1072#1081#1083
      object NOtkrit: TMenuItem
        Caption = #1054#1090#1082#1088#1099#1090#1100
        OnClick = NOtkritClick
      end
      object NVihod: TMenuItem
        Caption = #1042#1099#1093#1086#1076
        OnClick = NVihodClick
      end
    end
    object NAbout: TMenuItem
      Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077
      OnClick = NAboutClick
    end
  end
  object Query: TQuery
    Constrained = True
    RequestLive = True
    Left = 104
    Top = 56
  end
  object DataSource: TDataSource
    DataSet = Query
    Left = 184
    Top = 88
  end
  object OpenDialog: TOpenDialog
    Left = 64
    Top = 104
  end
end
