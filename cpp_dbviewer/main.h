//---------------------------------------------------------------------------

#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <DBCtrls.hpp>
#include <DBGrids.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <Menus.hpp>
#include <Dialogs.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------
class TDBViewerForm : public TForm
{
__published:	// IDE-managed Components
   TMainMenu *MainMenu;
   TMenuItem *A1;
   TMenuItem *NAbout;
   TMenuItem *NOtkrit;
   TMenuItem *NVihod;
   TQuery *Query;
   TDataSource *DataSource;
   TDBGrid *DBGrid;
   TDBNavigator *DBNavigator1;
   TOpenDialog *OpenDialog;
   void __fastcall FormCreate(TObject *Sender);
   void __fastcall NVihodClick(TObject *Sender);
   void __fastcall NOtkritClick(TObject *Sender);
   void __fastcall NAboutClick(TObject *Sender);
private:	// User declarations
   void opendb(){
      AnsiString db, dir;
      //��������� ���� �������
      OpenDialog->Execute();
      //���� ���� �������
      if (OpenDialog->FileName != ""){
         Query->Active = false;
         Query->SQL->Clear();
         //�������� �� ���
         db = ExtractFileName(OpenDialog->FileName);
         //�������� ����
         dir = ExtractFilePath(OpenDialog->FileName);
         //������ ������� ����������
         Query->DatabaseName = dir;
         //�������� ��� ���� �� ������� ����, ����� ������
         Query->SQL->Add("select * from " + db);
         //��������� ������
         Query->ExecSQL();
         //������� ���������
         DBGrid->Visible = true;
         Query->Active = true;
         }
      else{
         ShowMessage("�� �� ������� ������� ��� ��!!!");
         }
      };
public:		// User declarations
   __fastcall TDBViewerForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TDBViewerForm *DBViewerForm;
//---------------------------------------------------------------------------
#endif
