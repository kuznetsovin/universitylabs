//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Setting.h"
#include "Sys_ischisl.h"
#include <stdio.h>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TForm4 *Form4;
//---------------------------------------------------------------------------
__fastcall TForm4::TForm4(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm4::Button_OKClick(TObject *Sender)
{
        Form4->Close();

        set = fopen("setting.ini", "w+");
        fprintf(set, "%d \n", Form4->CSpinEdit_Tochn->Value);
        fprintf(set, "%d \n", Form4->CSpinEdit_log->Value);
        fclose(set);
}
//---------------------------------------------------------------------------
void __fastcall TForm4::Button_Och_logClick(TObject *Sender)
{
        TStringList *strg = new TStringList();

        strg->LoadFromFile("log.txt");
        strg->Clear();
        strg->SaveToFile("log.txt");
        Form1->Memo1->Lines->LoadFromFile("log.txt");
}
//---------------------------------------------------------------------------



void __fastcall TForm4::FormCreate(TObject *Sender)
{
        TStringList *strg = new TStringList();
        FILE *stg;
        int st;

        stg = fopen("setting.ini", "r");
        fscanf(stg, "%d \n", &st);
        Form4->CSpinEdit_Tochn->Value = st;
        fscanf(stg, "%d \n", &st);
        Form4->CSpinEdit_log->Value = st;
        fclose(stg);   

        strg->LoadFromFile("log.txt");
        if (Form4->CSpinEdit_log->Value < strg->Count)
        {
                strg->Clear();
                strg->SaveToFile("log.txt");
                Form1->Memo1->Lines->LoadFromFile("log.txt");
        }


}
//---------------------------------------------------------------------------

