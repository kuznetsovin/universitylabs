object Form4: TForm4
  Left = 281
  Top = 110
  BorderStyle = bsDialog
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
  ClientHeight = 293
  ClientWidth = 274
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 32
    Width = 201
    Height = 14
    Caption = #1058#1086#1095#1085#1086#1089#1090#1100' '#1087#1077#1088#1077#1074#1086#1076#1072' '#1076#1088#1086#1073#1085#1086#1081' '#1095#1072#1089#1090#1080
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 16
    Top = 104
    Width = 162
    Height = 14
    Caption = #1063#1080#1089#1083#1086' '#1079#1072#1087#1080#1089#1077#1081' '#1074' LOG-'#1092#1072#1081#1083#1077
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 16
    Top = 72
    Width = 139
    Height = 14
    Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' LOG-'#1092#1072#1081#1083#1072
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 16
    Top = 8
    Width = 131
    Height = 14
    Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1087#1077#1088#1077#1074#1086#1076#1072
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Button_OK: TButton
    Left = 152
    Top = 240
    Width = 81
    Height = 33
    Caption = 'OK'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = Button_OKClick
  end
  object Button_Och_log: TButton
    Left = 16
    Top = 136
    Width = 89
    Height = 33
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1083#1086#1075
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnClick = Button_Och_logClick
  end
  object CSpinEdit_log: TCSpinEdit
    Left = 184
    Top = 96
    Width = 41
    Height = 22
    MaxValue = 100
    TabOrder = 2
    Value = 20
  end
  object CSpinEdit_Tochn: TCSpinEdit
    Left = 224
    Top = 24
    Width = 41
    Height = 22
    MaxValue = 10
    TabOrder = 3
    Value = 4
  end
end
