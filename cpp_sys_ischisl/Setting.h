//---------------------------------------------------------------------------

#ifndef SettingH
#define SettingH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "CSPIN.h"
#include <stdio.h>
//---------------------------------------------------------------------------
class TForm4 : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TButton *Button_OK;
        TButton *Button_Och_log;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TCSpinEdit *CSpinEdit_log;
        TCSpinEdit *CSpinEdit_Tochn;
        void __fastcall Button_OKClick(TObject *Sender);
        void __fastcall Button_Och_logClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
private: FILE *log, *set;
         // User declarations
public:		// User declarations
        __fastcall TForm4(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm4 *Form4;
//---------------------------------------------------------------------------
#endif
