//---------------------------------------------------------------------------
#include <assert.h>
#include <vcl.h>
#pragma hdrstop

#include "About.h"
#include "Setting.h"
#include "Sys_ischisl.h"
#include "Sozd_sys_ischisl.h"
#include "Sozd_poluch_sys_ischisl.h"
#include <math.h>
#include <stdio.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
int stepen(int ch, int st)
{
        int res = 1, f;
        for(f = 0; f < st; f++) res = res * ch;
        return (res);
}

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ComboBox_Vibor_Chisel_SIChange(TObject *Sender)
{
        switch (Form1->ComboBox_Vibor_Chisel_SI->ItemIndex)
        {
                case 0:
                        chisla_si.Length = 2;
                        simvoli_si.Length = 2;
                        for (j = 0, i = '0';j < chisla_si.Length; j++, i++)
                        {
                                simvoli_si[j] = i;
                                chisla_si[j] = j;
                        }
                        break;
                case 1:
                        chisla_si.Length = 3;
                        simvoli_si.Length = 3;
                        for (j = 0, i = '0';j < chisla_si.Length; j++, i++)
                        {
                                simvoli_si[j] = i;
                                chisla_si[j] = j;
                        }
                        break;
                case 2:
                        chisla_si.Length = 4;
                        simvoli_si.Length = 4;
                        for (j = 0, i = '0';j < chisla_si.Length; j++, i++)
                        {
                                simvoli_si[j] = i;
                                chisla_si[j] = j;
                        }
                        break;
                case 3:
                        chisla_si.Length = 5;
                        simvoli_si.Length = 5;
                        for (j = 0, i = '0';j < chisla_si.Length; j++, i++)
                        {
                                simvoli_si[j] = i;
                                chisla_si[j] = j;
                        }
                        break;
                case 4:
                        chisla_si.Length = 6;
                        simvoli_si.Length = 6;
                        for (j = 0, i = '0';j < chisla_si.Length; j++, i++)
                        {
                                simvoli_si[j] = i;
                                chisla_si[j] = j;
                        }
                        break;
                case 5:
                        chisla_si.Length = 7;
                        simvoli_si.Length = 7;
                        for (j = 0, i = '0';j < chisla_si.Length; j++, i++)
                        {
                                simvoli_si[j] = i;
                                chisla_si[j] = j;
                        }
                        break;
                case 6:
                        chisla_si.Length = 8;
                        simvoli_si.Length = 8;
                        for (j = 0, i = '0';j < chisla_si.Length; j++, i++)
                        {
                                simvoli_si[j] = i;
                                chisla_si[j] = j;
                        }
                        break;
                case 7:
                        chisla_si.Length = 9;
                        simvoli_si.Length = 9;
                        for (j = 0, i = '0';j < chisla_si.Length; j++, i++)
                        {
                                simvoli_si[j] = i;
                                chisla_si[j] = j;
                        }
                        break;
                case 8:
                        chisla_si.Length = 10;
                        simvoli_si.Length = 10;
                        for (j = 0, i = '0';j < chisla_si.Length; j++, i++)
                        {
                                simvoli_si[j] = i;
                                chisla_si[j] = j;
                        }
                        break;
                case 9:
                        chisla_si.Length = 11;
                        simvoli_si.Length = 11;
                        for (j = 0, i = '0';j < chisla_si.Length; j++, i++)
                        {
                                 if (j == 10) i = 'A';
                                 simvoli_si[j] = i;
                                 chisla_si[j] = j;
                        }
                        break;
                case 10:
                        chisla_si.Length = 12;
                        simvoli_si.Length = 12;
                        for (j = 0, i = '0';j < chisla_si.Length; j++, i++)
                        {
                                if (j == 10) i = 'A';
                                simvoli_si[j] = i;
                                chisla_si[j] = j;
                        }
                        break;
                case 11:
                        chisla_si.Length = 13;
                        simvoli_si.Length = 13;
                        for (j = 0, i = '0';j < chisla_si.Length; j++, i++)
                        {
                                if (j == 10) i = 'A';
                                simvoli_si[j] = i;
                                chisla_si[j] = j;
                        }
                        break;
                case 12:
                        chisla_si.Length = 14;
                        simvoli_si.Length = 14;
                        for (j = 0, i = '0';j < chisla_si.Length; j++, i++)
                        {
                                if (j == 10) i = 'A';
                                simvoli_si[j] = i;
                                chisla_si[j] = j;
                        }
                        break;
                case 13:
                        chisla_si.Length = 15;
                        simvoli_si.Length = 15;
                        for (j = 0, i = '0';j < chisla_si.Length; j++, i++)
                        {
                                if (j == 10) i = 'A';
                                simvoli_si[j] = i;
                                chisla_si[j] = j;
                        }
                        break;
                case 14:
                        chisla_si.Length = 16;
                        simvoli_si.Length = 16;
                        for (j = 0, i = '0';j < chisla_si.Length; j++, i++)
                        {
                                if (j == 10) i = 'A';
                                simvoli_si[j] = i;
                                chisla_si[j] = j;
                        }
                        break;
                case 15:
                        Form2->Visible = true;
                        chisla_si.Length = 16 + Form2->Edit_Simvoli_si->Text.Length();
                        simvoli_si.Length = 16 + Form2->Edit_Simvoli_si->Text.Length();
                        for ( j = 0, i = '0', k = 0; j < chisla_si.Length; j++, i++)
                        {
                                if (j == 10) i = 'A';
                                if (j >= 16)
                                {
                                        i = (Form2->Edit_Simvoli_si->Text.c_str() )[k];
                                        k++;
                                }
                                simvoli_si[j] = i;
                                chisla_si[j] = j;
                        }
                        break;
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ComboBox_Poluch_SIChange(TObject *Sender)
{
        switch (Form1->ComboBox_Poluch_SI->ItemIndex)
        {
                case 0:
                        chisla_si_1.Length = 2;
                        simvoli_si_1.Length = 2;
                        for (j = 0, i = '0';j < chisla_si_1.Length; j++, i++)
                        {
                                simvoli_si_1[j] = i;
                                chisla_si_1[j] = j;
                        }
                        break;
                case 1:
                        chisla_si_1.Length = 3;
                        simvoli_si_1.Length = 3;
                        for (j = 0, i = '0';j < chisla_si_1.Length; j++, i++)
                        {
                                simvoli_si_1[j] = i;
                                chisla_si_1[j] = j;
                        }
                        break;
                case 2:
                        chisla_si_1.Length = 4;
                        simvoli_si_1.Length = 4;
                        for (j = 0, i = '0';j < chisla_si.Length; j++, i++)
                        {
                                simvoli_si_1[j] = i;
                                chisla_si_1[j] = j;
                        }
                        break;
                case 3:
                        chisla_si_1.Length = 5;
                        simvoli_si_1.Length = 5;
                        for (j = 0, i = '0';j < chisla_si_1.Length; j++, i++)
                        {
                                simvoli_si_1[j] = i;
                                chisla_si_1[j] = j;
                        }
                        break;
                case 4:
                        chisla_si_1.Length = 6;
                        simvoli_si_1.Length = 6;
                        for (j = 0, i = '0';j < chisla_si_1.Length; j++, i++)
                        {
                                simvoli_si_1[j] = i;
                                chisla_si_1[j] = j;
                        }
                        break;
                case 5:
                        chisla_si_1.Length = 7;
                        simvoli_si_1.Length = 7;
                        for (j = 0, i = '0';j < chisla_si_1.Length; j++, i++)
                        {
                                simvoli_si_1[j] = i;
                                chisla_si_1[j] = j;
                        }
                        break;
                case 6:
                        chisla_si_1.Length = 8;
                        simvoli_si_1.Length = 8;
                        for (j = 0, i = '0';j < chisla_si_1.Length; j++, i++)
                        {
                                simvoli_si_1[j] = i;
                                chisla_si_1[j] = j;
                        }
                        break;
                case 7:
                        chisla_si_1.Length = 9;
                        simvoli_si_1.Length = 9;
                        for (j = 0, i = '0';j < chisla_si_1.Length; j++, i++)
                        {
                                simvoli_si_1[j] = i;
                                chisla_si_1[j] = j;
                        }
                        break;
                case 8:
                        chisla_si_1.Length = 10;
                        simvoli_si_1.Length = 10;
                        for (j = 0, i = '0';j < chisla_si.Length; j++, i++)
                        {
                                simvoli_si_1[j] = i;
                                chisla_si_1[j] = j;
                        }
                        break;
                case 9:
                        chisla_si_1.Length = 11;
                        simvoli_si_1.Length = 11;
                        for (j = 0, i = '0';j < chisla_si_1.Length; j++, i++)
                        {
                                if (j == 10) i = 'A';
                                simvoli_si_1[j] = i;
                                chisla_si_1[j] = j;
                        }
                        break;
                case 10:
                        chisla_si_1.Length = 12;
                        simvoli_si_1.Length = 12;
                        for (j = 0, i = '0';j < chisla_si_1.Length; j++, i++)
                        {
                                if (j == 10) i = 'A';
                                simvoli_si_1[j] = i;
                                chisla_si_1[j] = j;
                        }
                        break;
                case 11:
                        chisla_si_1.Length = 13;
                        simvoli_si_1.Length = 13;
                        for (j = 0, i = '0';j < chisla_si_1.Length; j++, i++)
                        {
                                if (j == 10) i = 'A';
                                simvoli_si_1[j] = i;
                                chisla_si_1[j] = j;
                        }
                        break;
                case 12:
                        chisla_si_1.Length = 14;
                        simvoli_si_1.Length = 14;
                        for (j = 0, i = '0';j < chisla_si_1.Length; j++, i++)
                        {
                                if (j == 10) i = 'A';
                                simvoli_si_1[j] = i;
                                chisla_si_1[j] = j;
                        }
                        break;
                case 13:
                        chisla_si_1.Length = 15;
                        simvoli_si_1.Length = 15;
                        for (j = 0, i = '0';j < chisla_si_1.Length; j++, i++)
                        {
                                if (j == 10) i = 'A';
                                simvoli_si_1[j] = i;
                                chisla_si_1[j] = j;
                        }
                        break;
                case 14:
                        chisla_si_1.Length = 16;
                        simvoli_si_1.Length = 16;
                        for (j = 0, i = '0';j < chisla_si_1.Length; j++, i++)
                        {
                                if (j == 10) i = 'A';
                                simvoli_si_1[j] = i;
                                chisla_si_1[j] = j;
                        }
                        break;
                case 15:
                        Form3->Visible = true;
                        chisla_si_1.Length = 16 + Form3->Edit_Simvoli_si->Text.Length();
                        simvoli_si_1.Length = 16 + Form3->Edit_Simvoli_si->Text.Length();
                        for ( j = 0, i = '0', k = 0; j < chisla_si_1.Length; j++, i++)
                        {
                                if (j == 10) i = 'A';
                                if (j >= 16)
                                {
                                        i = (Form3->Edit_Simvoli_si->Text.c_str() )[k];
                                        k++;
                                }
                                simvoli_si_1[j] = i;
                                chisla_si_1[j] = j;
                        }
                        break;
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button_SchetClick(TObject *Sender)
{
        int c, l = 0;
        float desyat_si = 0;
        Form1->Edit_Otvet->Text = "";

        if (Form1->Edit_Chislo_Vvoda->Text.Length() <= 20)
        {
        if (Form1->Edit_Chislo_Vvoda->Text == "") ShowMessage("������� ����� ��� ��������");
        else
        {
                vvod_simvoli.Length = Form1->Edit_Chislo_Vvoda->Text.Length();
                for ( k = 0; k < vvod_simvoli.Length; k++ )
                        vvod_simvoli[k] = (Form1->Edit_Chislo_Vvoda->Text.c_str() )[k];


        if (Form1->ComboBox_Poluch_SI->ItemIndex == 15)
        {
                chisla_si_1.Length = 16 + Form3->Edit_Simvoli_si->Text.Length();
                simvoli_si_1.Length = 16 + Form3->Edit_Simvoli_si->Text.Length();
                for ( j = 0, i = '0', k = 0; j < chisla_si_1.Length; j++, i++)
                {
                        if (j == 10) i = 'A';
                        if (j >= 16)
                        {
                                i = (Form3->Edit_Simvoli_si->Text.c_str() )[k];
                                k++;
                        }
                simvoli_si_1[j] = i;
                chisla_si_1[j] = j;
                }
        }

        if (Form1->ComboBox_Vibor_Chisel_SI->ItemIndex == 15)
        {
                chisla_si.Length = 16 + Form2->Edit_Simvoli_si->Text.Length();
                simvoli_si.Length = 16 + Form2->Edit_Simvoli_si->Text.Length();
                for ( j = 0, i = '0', k = 0; j < chisla_si.Length; j++, i++)
                {
                        if (j == 10) i = 'A';
                        if (j >= 16)
                        {
                                i = (Form2->Edit_Simvoli_si->Text.c_str() )[k];
                                k++;
                        }
                simvoli_si[j] = i;
                chisla_si[j] = j;
                }
        }

        for ( k = 0; k < vvod_simvoli.Length; k++ )
        {
                j = 0;
                while (j < chisla_si.Length)
                {
                        if (vvod_simvoli[k] != simvoli_si[j])
                        {
                                c = 1;
                                j++;
                        }
                        else
                        {
                                c = 0;
                                break;
                        }
                }
        }
        if (c == 1)
                MessageDlg("������� ������� ����� � ������ ������� ����������",
                                mtError, TMsgDlgButtons() << mbOK, 0);
        else
        {
                if (Form1->ComboBox_Vibor_Chisel_SI->ItemIndex != 8)
                {
                        chislo.Length = vvod_simvoli.Length;
                        for ( k = 0; k < chislo.Length; k++ )
                        {
                               if (vvod_simvoli[k] == ',')
                               {
                                        c = k;
                                        l = 1;
                               }
                               else
                               {
                                        j = 0;
                                        while (j < chisla_si.Length)
                                                if (vvod_simvoli[k] == simvoli_si[j])
                                                {
                                                        chislo[k] = j;
                                                        break;
                                                }
                                                else j++;
                               }
                        }

                        int t;
                        if (l == 1)
                        {
                                t = c - 1;
                                for ( k = 0; k < c; k++ )
                                {
                                        desyat_si = desyat_si + chisla_si[chislo[k]] * stepen(chisla_si.Length,t);
                                        t--;
                                }

                                t = 1;
                                for ( k = c + 1; k < chislo.Length; k++ )
                                {
                                        desyat_si = desyat_si + chisla_si[chislo[k]] / stepen(chisla_si.Length,t);
                                        t++;
                                }
                        }

                        else
                        {
                                t = vvod_simvoli.Length - 1;
                                for ( k = 0; k < chislo.Length; k++ )
                                {
                                        desyat_si = desyat_si + chisla_si[chislo[k]] * stepen(chisla_si.Length,t);
                                        t--;
                                }
                        }
                }
                else
                        desyat_si = StrToFloat(Form1->Edit_Chislo_Vvoda->Text);


        if (Form1->ComboBox_Poluch_SI->ItemIndex == 8)
                Form1->Edit_Otvet->Text = FloatToStr(desyat_si);
        else
        {
        float  pomezh = 0;
        int ost, buf1;
        float buf2;

        buf1 = floorl(desyat_si);
        buf2 = desyat_si - buf1;
        k = 0;
        chislo.Length = 10000;
        while (buf1 >= chisla_si_1.Length)
        {
                pomezh = buf1 / chisla_si_1.Length;
                ost = buf1 - floorl(pomezh) * chisla_si_1.Length;
                buf1 = floorl(pomezh);
                j = 0;
                while (j < chisla_si_1.Length)
                {
                        if (ost == chisla_si_1[j])
                        {
                                chislo[k] = j;
                                k++;
                                break;
                        }
                        else j++;
                }

        }
        if ( buf1 < chisla_si_1.Length)
                {
                        j = 0;
                        while (j < chisla_si_1.Length)
                        {
                                if (buf1 == chisla_si_1[j])
                                {
                                        chislo[k] = j;
                                        break;
                                }
                                else j++;
                        }
                }

        chislo.Length = k + 1;
        while ( k >= 0 )
        {
                Form1->Edit_Otvet->Text = Form1->Edit_Otvet->Text + AnsiString(simvoli_si_1[chislo[k]]);
                k--;
        }


        if (buf2 != 0)
        {
                Form1->Edit_Otvet->Text = Form1->Edit_Otvet->Text + ",";
                chislo.Length = 10000;
                h = 0;
                while (h <= (Form4->CSpinEdit_Tochn->Value - 1))
                {
                        buf2 = buf2 * chisla_si_1.Length ;
                        ost = floorl(buf2);
                        buf2 = buf2 - ost;
                        j = 0;
                        while (j < chisla_si_1.Length)
                        {
                                if (ost == chisla_si_1[j])
                                {
                                        chislo[h] = j;
                                        Form1->Edit_Otvet->Text = Form1->Edit_Otvet->Text + AnsiString(simvoli_si_1[chislo[h]]);
                                        h++;
                                        break;
                                }
                                else j++;
                        }
                }
        }

        }
        }

        log = fopen("log.txt", "a+");
        fprintf(log, "%s %s %s %s %s %s %s %s \n", "�����", Form1->Edit_Chislo_Vvoda->Text, "��",
                 Form1->ComboBox_Vibor_Chisel_SI->Text, "�", Form1->ComboBox_Poluch_SI->Text, "�����", Form1->Edit_Otvet->Text);
        fclose(log);
        Form1->Memo1->Lines->LoadFromFile("log.txt");

        }
        }
        else MessageDlg("C������ ������� �����", mtError, TMsgDlgButtons() << mbOK, 0);


}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
        chisla_si.Length = 2;
        simvoli_si.Length = 2;
        for (j = 0, i = '0';j < chisla_si.Length; j++, i++)
        {
                simvoli_si[j] = i;
                chisla_si[j] = j;
        }

        chisla_si_1.Length = 2;
        simvoli_si_1.Length = 2;
        for (j = 0, i = '0';j < chisla_si.Length; j++, i++)
        {
                simvoli_si_1[j] = i;
                chisla_si_1[j] = j;
        }

        log = fopen("log.txt", "a");
        fclose(log);
        Form1->Memo1->Lines->LoadFromFile("log.txt");

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
        AboutBox->Visible = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
        Form4->Visible = true;
}
//---------------------------------------------------------------------------

