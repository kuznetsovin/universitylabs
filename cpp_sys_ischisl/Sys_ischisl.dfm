object Form1: TForm1
  Left = 261
  Top = 101
  Anchors = [akTop, akRight]
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #1057#1080#1089#1090#1077#1084#1099' '#1080#1089#1095#1080#1089#1083#1077#1085#1080#1103
  ClientHeight = 414
  ClientWidth = 428
  Color = clWhite
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWhite
  Font.Height = -13
  Font.Name = 'System'
  Font.Style = [fsBold]
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object Label2: TLabel
    Left = 16
    Top = 56
    Width = 105
    Height = 14
    Caption = #1050#1086#1085#1077#1095#1085#1072#1103' '#1089#1080#1089#1090#1077#1084#1072
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 16
    Top = 24
    Width = 110
    Height = 14
    Caption = #1048#1089#1093#1086#1076#1085#1091#1102' '#1089#1080#1089#1090#1077#1084#1072' '
    Color = clWhite
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label4: TLabel
    Left = 16
    Top = 88
    Width = 154
    Height = 14
    Caption = #1063#1080#1089#1083#1086' '#1074' '#1080#1089#1093#1086#1076#1085#1086#1081' '#1089#1080#1089#1090#1077#1084#1077
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 16
    Top = 120
    Width = 155
    Height = 14
    Caption = #1063#1080#1089#1083#1086' '#1074' '#1082#1086#1085#1077#1095#1085#1086#1081' '#1089#1080#1089#1090#1077#1084#1077
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 24
    Top = 232
    Width = 138
    Height = 14
    Caption = #1046#1091#1088#1085#1072#1083' '#1089#1086#1073#1099#1090#1080#1081' ('#1083#1086#1075') :'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Bevel1: TBevel
    Left = 16
    Top = 152
    Width = 393
    Height = 1
    Shape = bsTopLine
  end
  object Bevel2: TBevel
    Left = 16
    Top = 216
    Width = 393
    Height = 1
    Shape = bsTopLine
  end
  object Edit_Chislo_Vvoda: TEdit
    Left = 216
    Top = 80
    Width = 193
    Height = 22
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object Edit_Otvet: TEdit
    Left = 216
    Top = 112
    Width = 193
    Height = 22
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object ComboBox_Vibor_Chisel_SI: TComboBox
    Left = 216
    Top = 16
    Width = 193
    Height = 20
    Style = csOwnerDrawVariable
    Ctl3D = True
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemHeight = 14
    ItemIndex = 0
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 2
    Text = #1044#1074#1086#1080#1095#1085#1072#1103
    OnChange = ComboBox_Vibor_Chisel_SIChange
    Items.Strings = (
      #1044#1074#1086#1080#1095#1085#1072#1103
      #1058#1088#1086#1080#1095#1085#1072#1103
      #1063#1077#1090#1074#1077#1088#1080#1095#1085#1072#1103
      #1055#1103#1090#1080#1088#1080#1095#1085#1072#1103
      #1064#1077#1089#1090#1080#1088#1080#1095#1085#1072#1103
      #1057#1077#1084#1080#1088#1080#1095#1085#1072#1103
      #1042#1086#1089#1100#1084#1080#1088#1080#1095#1085#1072#1103
      #1044#1077#1074#1103#1090#1080#1088#1080#1095#1085#1072#1103
      #1044#1077#1089#1103#1090#1080#1088#1080#1095#1085#1072#1103
      #1054#1076#1080#1085#1072#1076#1094#1072#1090#1080#1088#1080#1095#1085#1072#1103
      #1044#1074#1077#1085#1072#1076#1094#1072#1090#1080#1088#1080#1095#1085#1072#1103
      #1058#1088#1080#1085#1072#1076#1094#1072#1090#1080#1088#1080#1095#1085#1072#1103
      #1063#1077#1090#1099#1088#1085#1072#1076#1094#1072#1090#1080#1088#1080#1095#1085#1072#1103
      #1055#1103#1090#1085#1072#1076#1094#1072#1090#1080#1088#1080#1095#1085#1072#1103
      #1064#1077#1089#1085#1072#1076#1094#1072#1090#1080#1088#1080#1095#1085#1072#1103
      #1044#1088#1091#1075#1072#1103' ...')
  end
  object ComboBox_Poluch_SI: TComboBox
    Left = 216
    Top = 48
    Width = 193
    Height = 22
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemHeight = 14
    ItemIndex = 0
    ParentFont = False
    TabOrder = 3
    Text = #1044#1074#1086#1080#1095#1085#1072#1103
    OnChange = ComboBox_Poluch_SIChange
    Items.Strings = (
      #1044#1074#1086#1080#1095#1085#1072#1103
      #1058#1088#1086#1080#1095#1085#1072#1103
      #1063#1077#1090#1074#1077#1088#1080#1095#1085#1072#1103
      #1055#1103#1090#1080#1088#1080#1095#1085#1072#1103
      #1064#1077#1089#1090#1080#1088#1080#1095#1085#1072#1103
      #1057#1077#1084#1080#1088#1080#1095#1085#1072#1103
      #1042#1086#1089#1100#1084#1080#1088#1080#1095#1085#1072#1103
      #1044#1077#1074#1103#1090#1080#1088#1080#1095#1085#1072#1103
      #1044#1077#1089#1103#1090#1080#1088#1080#1095#1085#1072#1103
      #1054#1076#1080#1085#1072#1076#1094#1072#1090#1080#1088#1080#1095#1085#1072#1103
      #1044#1074#1077#1085#1072#1076#1094#1072#1090#1080#1088#1080#1095#1085#1072#1103
      #1058#1088#1080#1085#1072#1076#1094#1072#1090#1080#1088#1080#1095#1085#1072#1103
      #1063#1077#1090#1099#1088#1085#1072#1076#1094#1072#1090#1080#1088#1080#1095#1085#1072#1103
      #1055#1103#1090#1085#1072#1076#1094#1072#1090#1080#1088#1080#1095#1085#1072#1103
      #1064#1077#1089#1085#1072#1076#1094#1072#1090#1080#1088#1080#1095#1085#1072#1103
      #1044#1088#1091#1075#1072#1103' ...')
  end
  object Button_Schet: TBitBtn
    Left = 16
    Top = 168
    Width = 121
    Height = 33
    Caption = #1042#1099#1095#1080#1089#1083#1080#1090#1100
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    OnClick = Button_SchetClick
    Style = bsWin31
  end
  object Button1: TButton
    Left = 152
    Top = 168
    Width = 121
    Height = 33
    Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 288
    Top = 168
    Width = 121
    Height = 33
    Caption = #1040#1074#1090#1086#1088#1099
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    OnClick = Button2Click
  end
  object Memo1: TMemo
    Left = 24
    Top = 256
    Width = 377
    Height = 137
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Lines.Strings = (
      '')
    ParentFont = False
    ParentShowHint = False
    ScrollBars = ssVertical
    ShowHint = True
    TabOrder = 7
  end
end
