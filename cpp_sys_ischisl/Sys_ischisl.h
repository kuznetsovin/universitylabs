//---------------------------------------------------------------------------

#ifndef Sys_ischislH
#define Sys_ischislH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <Dialogs.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <stdio.h>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TEdit *Edit_Chislo_Vvoda;
        TLabel *Label5;
        TEdit *Edit_Otvet;
        TComboBox *ComboBox_Vibor_Chisel_SI;
        TComboBox *ComboBox_Poluch_SI;
        TBitBtn *Button_Schet;
        TButton *Button1;
        TButton *Button2;
        TMemo *Memo1;
        TLabel *Label1;
        TBevel *Bevel1;
        TBevel *Bevel2;
        void __fastcall ComboBox_Vibor_Chisel_SIChange(TObject *Sender);
        void __fastcall Button_SchetClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall ComboBox_Poluch_SIChange(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		DynamicArray<char> simvoli_si;
                DynamicArray<char> simvoli_si_1;
                DynamicArray<int> chisla_si_1;
                DynamicArray<float> chisla_si, chislo, simvol,a;
                DynamicArray<char> vvod_simvoli;
                int j, k, h;
                char i;
                float z;
                FILE *log;
                // User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
