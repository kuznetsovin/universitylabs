program QuickTest;



uses
  Forms,
  Enter in 'src\Enter.pas' {F_Enter},
  SelectFunctionalSubsystem in 'src\SelectFunctionalSubsystem.pas' {F_SelectFunctionalSubsystem},
  SelectDiscipline in 'src\SelectDiscipline.pas' {F_SelectDiscipline},
  Global in 'src\Global.pas',
  Login in 'src\Login.pas' {F_Login},
  QuestionList in 'src\QuestionList.pas' {F_QuestionList},
  Question in 'src\Question.pas' {F_Question},
  Result in 'src\Result.pas' {F_Result},
  SelectMove in 'src\SelectMove.pas' {F_SelectMove};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TF_Enter, F_Enter);
  Application.CreateForm(TF_SelectFunctionalSubsystem, F_SelectFunctionalSubsystem);
  Application.CreateForm(TF_SelectDiscipline, F_SelectDiscipline);
  Application.CreateForm(TF_Login, F_Login);
  Application.CreateForm(TF_QuestionList, F_QuestionList);
  Application.CreateForm(TF_Question, F_Question);
  Application.CreateForm(TF_Result, F_Result);
  Application.CreateForm(TF_SelectMove, F_SelectMove);
  Application.Run;
end.
