object F_Enter: TF_Enter
  Left = 0
  Top = 0
  BorderStyle = bsNone
  ClientHeight = 328
  ClientWidth = 540
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object L_MainMessage: TLabel
    Left = 0
    Top = 48
    Width = 540
    Height = 169
    Align = alCustom
    Alignment = taCenter
    AutoSize = False
    Caption = 
      #1050#1086#1075#1085#1080#1090#1080#1074#1085#1072#1103' '#1089#1080#1089#1090#1077#1084#1072' '#1086#1073#1091#1095#1077#1085#1080#1103', '#1076#1080#1072#1075#1085#1086#1089#1090#1080#1082#1080' '#1080' '#1082#1086#1085#1090#1088#1086#1083#1103' '#1087#1088#1086#1092#1077#1089#1089#1080#1086#1085#1072 +
      #1083#1100#1085#1099#1093' '#1082#1086#1084#1087#1080#1090#1077#1085#1094#1080#1081', '#1085#1077#1086#1073#1093#1086#1076#1080#1084#1099#1093' '#1076#1083#1103' '#1091#1087#1088#1072#1074#1083#1077#1085#1080#1103' '#1101#1082#1086#1085#1086#1084#1080#1082#1086#1081' '#1079#1085#1072#1085#1080#1081
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    WordWrap = True
  end
  object L_EnterMessage: TLabel
    Left = 152
    Top = 296
    Width = 250
    Height = 14
    Caption = #1044#1083#1103' '#1087#1088#1086#1076#1086#1083#1085#1077#1085#1080#1103' '#1085#1072#1078#1084#1080#1090#1077' '#1083#1102#1073#1091#1102' '#1082#1083#1072#1074#1080#1096#1091
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
end
