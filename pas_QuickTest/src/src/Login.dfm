object F_Login: TF_Login
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'F_Login'
  ClientHeight = 240
  ClientWidth = 442
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'System'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 16
  object L_Fam: TLabel
    Left = 56
    Top = 24
    Width = 62
    Height = 16
    Caption = #1060#1072#1084#1080#1083#1080#1103
  end
  object L_Name: TLabel
    Left = 56
    Top = 56
    Width = 26
    Height = 16
    Caption = #1048#1084#1103
  end
  object L_Otchestvo: TLabel
    Left = 56
    Top = 86
    Width = 61
    Height = 16
    Caption = #1054#1090#1095#1077#1089#1090#1074#1086
  end
  object E_Fam: TEdit
    Left = 136
    Top = 24
    Width = 185
    Height = 24
    TabOrder = 0
  end
  object E_Name: TEdit
    Left = 136
    Top = 56
    Width = 185
    Height = 24
    TabOrder = 1
  end
  object E_Otchestvo: TEdit
    Left = 136
    Top = 86
    Width = 185
    Height = 24
    TabOrder = 2
  end
  object RB_Mode_auto: TRadioButton
    Left = 136
    Top = 128
    Width = 193
    Height = 17
    Caption = #1040#1074#1090#1086#1084#1072#1090#1080#1095#1077#1089#1082#1080#1081' '#1088#1077#1078#1080#1084
    TabOrder = 3
  end
  object RB_Mode_hand: TRadioButton
    Left = 136
    Top = 151
    Width = 129
    Height = 17
    Caption = #1056#1091#1095#1085#1086#1081' '#1088#1077#1078#1080#1084
    TabOrder = 4
  end
  object B_OK: TButton
    Left = 168
    Top = 184
    Width = 97
    Height = 25
    Caption = 'OK'
    TabOrder = 5
    OnClick = B_OKClick
  end
end
