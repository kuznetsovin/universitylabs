unit Question;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, ADODB, Global, Grids, DBGrids;

type
  TF_Question = class(TForm)
    B_Next: TButton;
    DS_Question: TDataSource;
    ADOQ_Question: TADOQuery;
    procedure FormShow(Sender: TObject);
    procedure B_NextClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_Question: TF_Question;
  QueryString: String;
  L_Question: TLabel;
  QuestionCount: Integer;
  AnswerVariant: array of TCheckBox;
  NomQuestions: array  of Integer;
  MaxQuestionCount: Integer;


implementation

uses Result;
{$R *.dfm}

//��������� ������ ������� �� �����
procedure PrintQuestion(var ADO_Query: TADOQuery);
var VariantCount: integer;
    TopCB: integer;
begin
L_Question := TLabel.Create(F_Question);
//������� ��� �� ����� � ��������� ������������
L_Question.Top := 15;
L_Question.Left := 15;
L_Question.Parent := F_Question;
//����������� ����
L_Question.WordWrap := True;
L_Question.Width := 525;

//�������� ������
ADO_Query.SQL.Clear;
QueryString := 'select question from QuestionCard' +
    IntToStr(NomQuestionCard) +
    ' where type="0" and numquestion="' +
    IntToStr(NomQuestions[QuestionCount]) + '"';
ADO_Query.SQL.Add(QueryString);
ADO_Query.ExecSQL;

//������� ������ �� �����
ADO_Query.Active := true;
L_Question.Caption := ADO_Query.FieldByName('question').AsString;

//�������� �������� �������
ADO_Query.Active := False;
ADO_Query.SQL.Clear;
QueryString := 'select left(type,1) as vvar, question from QuestionCard' +
    IntToStr(NomQuestionCard) + ' where type<>"0" and numquestion="' +
    IntToStr(NomQuestions[QuestionCount]) + '"';
ADO_Query.SQL.Add(QueryString);
ADO_Query.ExecSQL;

//������� �������� �������
ADO_Query.Active := true;
//����� ��������
VariantCount := 0;
//��������� �� ������ ������� 
ADO_Query.First;
TopCB := L_Question.Height + L_Question.Top + 10;
SetLength(AnswerVariant, ADO_Query.RecordCount);
while not(ADO_Query.Eof) do begin
    //������ ����������  ��������
    AnswerVariant[VariantCount] := TCheckBox.Create(F_Question);
    AnswerVariant[VariantCount].Top := TopCB;
    AnswerVariant[VariantCount].Left := 30;
    AnswerVariant[VariantCount].Parent := F_Question;
    //������ ������� �������� � �����������
    AnswerVariant[VariantCount].WordWrap := True;
    AnswerVariant[VariantCount].Height := 45;
    AnswerVariant[VariantCount].Width := 500;
    AnswerVariant[VariantCount].Enabled := true;
    AnswerVariant[VariantCount].Name := ADO_Query.FieldByName('vvar').AsString;
    //�������� ������� �� �����
    AnswerVariant[VariantCount].Caption := ADO_Query.FieldByName('question').AsString;
    //��������� � ���������� ��������
    ADO_Query.Next;
    TopCB := TopCB + 50;
    //������ ����� ��������
    VariantCount := VariantCount + 1;
    end;
end;


//������ ������� ���������� ���� ��������� �����
function Unic(var flag: array of boolean; range: integer): integer;
begin
  result := random(range);
  while flag[result] do
    //���� ������ ����� ��� ���
    result := random(range);
  //��� ����� �� ���� ����������
  flag[result] := true;
end;

procedure MassRand(range: integer; var inputMass: array of integer);
var
  i: integer;
  bm: array of boolean;
begin
  SetLength(bm, length(inputMass));
  for i := 0 to length(inputMass) - 1 do
  begin
    //��� ������������������ 1,2, ... , N
    inputMass[i] := Unic(bm, range) + 1;
  end;
end;



procedure TF_Question.B_NextClick(Sender: TObject);
var i: Integer;
    pvar, vvar: string;
begin

//�������� ����� �������� �������
ADOQ_Question.SQL.Clear;
ADOQ_Question.SQL.Add('select left(type,1) as pvar from QuestionCard' + IntToStr(NomQuestionCard) +
  ' where right(type,1) = "+" and numquestion="' + IntToStr(NomQuestions[QuestionCount]) + '"');
ADOQ_Question.ExecSQL;
ADOQ_Question.Active := true;
//���� ���� ���������� �����
if ADOQ_Question.RecordCount = 1 then begin
  pvar := ' ' + ADOQ_Question.FieldByName('pvar').AsString;
  end
else begin
  //���� ���������
  while not ADOQ_Question.Eof do  begin
    //���������� �� � ������
    pvar := pvar + ' ' + ADOQ_Question.FieldByName('pvar').AsString;
    ADOQ_Question.Next;
    end;
  end;

//�������� ��������� ������
for i := 0 to Length(AnswerVariant)-1 do begin
  if AnswerVariant[i].Checked then begin
    //���������� ��������� �������� � ������
    vvar := vvar + ' ' + AnswerVariant[i].Name;
    end;
  end;

//���������� ���������� ��� �������� �����������
ADOQ_Question.SQL.Clear;
ADOQ_Question.SQL.Add('insert into tmp (nqst, pvar, vvar) values ("' + IntToStr(NomQuestions[QuestionCount]) +
  '","' + pvar + '","' + vvar + '")');
ADOQ_Question.ExecSQL;

//������� ����� ��� ���������� �������
for i := 0 to Length(AnswerVariant)-1 do AnswerVariant[i].Free;
L_Question.Free;

//����������� ������� ��������
QuestionCount := QuestionCount + 1;

//������� ������
PrintQuestion(ADOQ_Question);
if QuestionCount = MaxQuestionCount then begin
  //���������� ����� �����������
  F_Result.Show;
  //��������� ����� ��������
  F_Question.Close;
  end;
end;

procedure TF_Question.FormShow(Sender: TObject);
begin
//������������� ������ �������
QueryString := '';
//������������� �������� ��������
QuestionCount := 0;

//--------�������--------------
//for i := 1 to 10 do showmessage(IntToStr(NomQuestions[i]));
//NomQuestionCard := 1;
//sg_PathToTest := 'D:\Documents\!Project\QuickTest\db\MSFO.mdb';
//����������� � �� ��������
ADOQ_Question.ConnectionString := 'Provider=Microsoft.Jet.OLEDB.4.0;' +
    'Data Source=' + sg_PathToTest +
    ';Persist Security Info=False';

//�������� ������ ��������
ADOQ_Question.SQL.Clear;
ADOQ_Question.SQL.Add('select distinct numquestion from QuestionCard' + IntToStr(NomQuestionCard));
ADOQ_Question.ExecSQL;

//�������� ���������� ��������
ADOQ_Question.Active := true;
MaxQuestionCount := ADOQ_Question.RecordCount;
ADOQ_Question.Active := false;

SetLength(NomQuestions, MaxQuestionCount);


//�������� ��������� ��������� �����
Randomize();
//������ ������� ��������
MassRand(MaxQuestionCount, NomQuestions);

//������� ������
PrintQuestion(ADOQ_Question);

ADOQ_Question.SQL.Clear;
ADOQ_Question.SQL.Add('create table tmp (`nqst` char(2), `pvar` char(10), `vvar` char(10))');
ADOQ_Question.ExecSQL;






//ADOQ_Question.SQL.Clear;
//ADOQ_Question.SQL.Add('update tmp set pvar=substring(pvar,1,1))');
//ADOQ_Question.ExecSQL;


//------�������-------------
//ADOQ_Question.Active := true;
//ADOQ_Question.First;
//while not(ADOQ_Question.Eof) do begin
//    ShowMessage(ADOQ_Question.FieldByName('type').AsString);
//    ADOQ_Question.Next;
//    end;
//L_Question.Caption := ADOQ_Question.FieldByName('type').AsString;
//ShowMessage(IntToStr(ADOQ_Question.RecordCount));
end;

end.
