unit Result;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Math,
  Dialogs, ImgList, ComCtrls, ToolWin, OleCtrls, SHDocVw, ActiveX, Global, Printers;

type
  TF_Result = class(TForm)
    WB_Result: TWebBrowser;
    TB_Print: TToolBar;
    TBtn_Print: TToolButton;
    ImageList1: TImageList;
    TBtn_Save: TToolButton;
    TBtn_Next: TToolButton;
    SD_Result: TSaveDialog;
    procedure TBtn_SaveClick(Sender: TObject);
    procedure TBtn_PrintClick(Sender: TObject);
    procedure TBtn_NextClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_Result: TF_Result;

implementation

uses Question, SelectMove;

{$R *.dfm}

 function WB_SaveHTMLCode(WebBrowser: TWebBrowser; const FileName: TFileName): Boolean;
 var
   ps: IPersistStreamInit;
   fs: TFileStream;
   sa: IStream;
 begin
   ps := WebBrowser.Document as IPersistStreamInit;
   fs := TFileStream.Create(FileName, fmCreate);
   try
     sa := TStreamAdapter.Create(fs, soReference) as IStream;
     Result := Succeeded(ps.Save(sa, True));
   finally
     fs.Free;
   end;
 end;

procedure WB_LoadHTML(WebBrowser: TWebBrowser; HTMLCode: string);
var
  sl: TStringList;
  ms: TMemoryStream;
begin
  WebBrowser.Navigate('about:blank');
  while WebBrowser.ReadyState < READYSTATE_INTERACTIVE do
    Application.ProcessMessages;

  if Assigned(WebBrowser.Document) then
  begin
    sl := TStringList.Create;
    try
      ms := TMemoryStream.Create;
      try
        sl.Text := HTMLCode;
        sl.SaveToStream(ms);
        ms.Seek(0, 0);
        (WebBrowser.Document as
          IPersistStreamInit).Load(TStreamAdapter.Create(ms));
      finally
        ms.Free;
      end;
    finally
      sl.Free;
    end;
  end;
end;

procedure TF_Result.FormShow(Sender: TObject);
var s_HTMLReport: string;  //� ���������� ������������ ��� html �������� ������
    I: Integer;
    procent: real;
    ball: string;
begin
//sg_FIO := '�������� �. �.';

//sg_NameDiscipline := '����';

s_HTMLReport := '<head>' + #13 +
'<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">' +
'<title>��������� ������</title>' +  #13 +
'</head>' +  #13 +
'<body>' + #13 +
'<div style="text-align: center; font-weight: bold;">' + #13 +
'<h2>������� ����������� ����� �� ����������&nbsp;</h2>'+ #13 +
'<h2>&laquo;' + sg_NameDiscipline + '&raquo;</h2>' + #13 +
'<table style="text-align: left; width: 977px; height: 145px;" border="1" cellpadding="0" cellspacing="0">' + #13 +
'  <tbody>' + #13 +
'    <tr>' + #13 +
'      <td style="text-align: center; font-family: Arial; height: 29px;"  ' + #13 +
' colspan="1" rowspan="2" valign="undefined">' + #13 +
'      <h3>���</h3>' + #13 +
'      </td>' + #13 +
'      <td style="text-align: center; height: 34px;"' + #13 +
'colspan="10" rowspan="1" valign="undefined">' + #13 +
'      <h3><span style="font-family: Arial;">�������</span></h3>' + #13 +
'      </td>' + #13 +
'    </tr>' + #13 +
'    <tr>' + #13 +
'      <td style="width: 75px; font-family: Arial; height: 29px; text-align: center;">' + #13 +
'      <h3>1</h3>' + #13 +
'      </td>' + #13 +
'      <td style="width: 75px; font-family: Arial; height: 29px; text-align: center;">' + #13 +
'      <h3>2</h3>' + #13 +
'      </td>' + #13 +
'      <td style="width: 75px; font-family: Arial; height: 29px; text-align: center;">' + #13 +
'      <h3>3</h3>' + #13 +
'      </td>' + #13 +
'      <td  style="width: 75px; font-family: Arial; height: 29px; text-align: center;">' + #13 +
'      <h3>4</h3>' + #13 +
'      </td>' + #13 +
'      <td style="width: 75px; font-family: Arial; height: 29px; text-align: center;">' + #13 +
'      <h3>5</h3>' + #13 +
'      </td>' + #13 +
'      <td style="width: 75px; font-family: Arial; height: 29px; text-align: center;">' + #13 +
'      <h3>6</h3>' + #13 +
'      </td>' + #13 +
'      <td  style="width: 75px; font-family: Arial; height: 29px; text-align: center;">' + #13 +
'      <h3>7</h3>' + #13 +
'      </td>' + #13 +
'      <td style="width: 75px; font-family: Arial; height: 29px; text-align: center;">' + #13 +
'      <h3>8</h3>' + #13 +
'      </td>' + #13 +
'      <td style="width: 75px; font-family: Arial; height: 29px; text-align: center;">' + #13 +
'      <h3>9</h3>' + #13 +
'      </td>' + #13 +
'      <td style="width: 75px; font-family: Arial; height: 29px; text-align: center;">' + #13 +
'      <h3>10</h3>' + #13 +
'      </td>' + #13 +
'    </tr>' + #13 +
'    <tr>' + #13 +
'      <td style="width: 203px; height: 41px;" colspan="1"' + #13 +
' rowspan="2" align="undefined" valign="undefined">&nbsp;' + sg_FIO + '</td>' + #13;
for I := 0 to 9 do begin
    //�������� ���������� ������
  F_Question.ADOQ_Question.SQL.Clear;
  F_Question.ADOQ_Question.SQL.Add('select pvar from tmp where nqst = "' + IntToStr(I + 1) + '"');
  F_Question.ADOQ_Question.ExecSQL;
  F_Question.ADOQ_Question.Active := true;
  s_HTMLReport := s_HTMLReport +
    '      <td style="width: 75px; height: 41px; text-align: center;" valign="undefined">&nbsp;' +
    F_Question.ADOQ_Question.FieldByName('pvar').AsString + '</td>' + #13;
  end;

s_HTMLReport := s_HTMLReport + '    </tr>' + #13 + '    <tr>' + #13;
for I := 0 to 9 do begin
  //�������� ��������� ������������� ������
  F_Question.ADOQ_Question.SQL.Clear;
  F_Question.ADOQ_Question.SQL.Add('select vvar from tmp where nqst = "' + IntToStr(I + 1) + '"');
  F_Question.ADOQ_Question.ExecSQL;
  F_Question.ADOQ_Question.Active := true;
  s_HTMLReport := s_HTMLReport +
    '      <td style="width: 75px; height: 41px; text-align: center;" valign="undefined">&nbsp;' +
    F_Question.ADOQ_Question.FieldByName('vvar').AsString + '</td>' + #13;
  end;

s_HTMLReport := s_HTMLReport + '    </tr>' + #13 +
'  </tbody>' + #13 +
'</table>' + #13 +
'<br>' + #13 +
'<div style="text-align: left;">' + #13;

//������� ������ ������ �������
F_Question.ADOQ_Question.SQL.Clear;
F_Question.ADOQ_Question.SQL.Add('select count(nqst) as procent from tmp where pvar = vvar');
F_Question.ADOQ_Question.ExecSQL;
F_Question.ADOQ_Question.Active := true;
procent := F_Question.ADOQ_Question.FieldByName('procent').AsInteger / QuestionCount * 100;
case Round(procent) of
  50..69: ball := '�����������������';
  70..90: ball := '������';
  91..100: ball := '�������';
  else ball := '�������������������';
end;

s_HTMLReport := s_HTMLReport +
'<h3><span style="font-family: Arial;">������� ������ �������:&nbsp;' + FloatToStr(RoundTo(procent, -2)) + '%</span></h3>' + #13 +
'<h3><span style="font-family: Arial;">������:&nbsp;' + ball + '</span></h3>' + #13 +
'</div>' + #13 +
'</div>' + #13 +
'</body>' + #13 +
'</html>';
WB_LoadHTML(WB_Result, s_HTMLReport);

F_Question.ADOQ_Question.SQL.Clear;
F_Question.ADOQ_Question.SQL.Add('drop table tmp');
F_Question.ADOQ_Question.ExecSQL;
//F_Question.ADOQ_Question.Destroy;
end;

procedure TF_Result.TBtn_NextClick(Sender: TObject);
begin
F_SelectMove.ShowModal;
F_Result.Close;
end;

procedure TF_Result.TBtn_PrintClick(Sender: TObject);
var
  vaIn, vaOut: OleVariant;
begin
  //����� �������� ��������
  WB_Result.ControlInterface.ExecWB(OLECMDID_PAGESETUP, OLECMDEXECOPT_PROMPTUSER,
    vaIn, vaOut);
  //������
  WB_Result.ControlInterface.ExecWB(OLECMDID_PRINT, OLECMDEXECOPT_DONTPROMPTUSER,
    vaIn, vaOut);
end;

procedure TF_Result.TBtn_SaveClick(Sender: TObject);
begin
SD_Result.Execute;
if SD_Result.FileName <> '' then begin
  WB_SaveHTMLCode(WB_Result, SD_Result.FileName + '.html');
  end
else ShowMessage ('�� ������� ��� �����');

end;

end.
