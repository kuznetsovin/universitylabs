unit SelectDiscipline;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Global, IniFiles, Login;

type
  TF_SelectDiscipline = class(TForm)
    LB_DisciplinesList: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure LB_DisciplinesListDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_SelectDiscipline: TF_SelectDiscipline;

implementation

uses QuestionList, SelectMove;

{$R *.dfm}


procedure TF_SelectDiscipline.FormCreate(Sender: TObject);
var FileSetting: TIniFile;
begin
//���� ��� ����� � ����������� ���������
if not FileExists(ExtractFilePath(Application.ExeName) +
  'DisciplineSetting.ini') then begin
    //������� ��������� �� ������
    MessageDlg('�� ������ ���� � ����������� ���������! ' +
      '��������� ����� �������.', mtError, [mbOk], 0);
    //��������� ���������
    Application.Terminate();
  end
else begin
  //����������� ���� �������� � ����������
  FileSetting := TIniFile.Create(ExtractFilePath(Application.ExeName) +
    'DisciplineSetting.ini');
  //��������� �������� ���������(������) �� ����� ��������
  FileSetting.ReadSections(LB_DisciplinesList.Items);
  //���������� ���������� ���������� � ������
  FileSetting.Destroy;
  end;
end;

procedure TF_SelectDiscipline.LB_DisciplinesListDblClick(Sender: TObject);
var FileSetting: TIniFile;
begin
//---�������------
//ShowMessage(LB_DisciplinesList.Items.Strings[LB_DisciplinesList.ItemIndex]);
//bg_SelectRate := True;

//���� ������ ����� ������������
if bg_SelectRate then begin
    //����������� ���� �������� � ����������
    FileSetting := TIniFile.Create(ExtractFilePath(Application.ExeName) +
        'DisciplineSetting.ini');
    //��������� ���� �� �� �� ����� ��������
    sg_PathToTest := FileSetting.ReadString(LB_DisciplinesList.Items.Strings[LB_DisciplinesList.ItemIndex],
        'PathToTest', '');
    //��������� ���������� ��������
    ig_QuestionCount := FileSetting.ReadInteger(LB_DisciplinesList.Items.Strings[LB_DisciplinesList.ItemIndex],
        'ColQuestionCard', 0);

    //�������� ����������
    sg_NameDiscipline := LB_DisciplinesList.Items.Strings[LB_DisciplinesList.ItemIndex];
    //---�������------
    //ShowMessage(sg_ConnectStr);

    //����� �����������
    if (ig_QuestionCount = 0) and (sg_PathToTest = '') then begin
      ShowMessage('����� ��������� ���������� � ����������. �������� ������ ����������!');
      F_SelectMove.Show;
      F_SelectMove.L_Login.Visible := false;
      F_SelectMove.L_NextQuestionCard.Visible := false;
      F_SelectDiscipline.Close;
      end
    else begin
      //���������� ���������� ���������� � ������
      FileSetting.Destroy;
      //��������� � ����� ������
      F_Login.Show;
      //��������� ���� ������ ��������
      F_SelectDiscipline.Close;
      end;
    end
else begin
  ShowMessage('����� ��������� ���������� � ����������.');
  F_SelectMove.Show;
  F_SelectMove.L_Login.Visible := false;
  F_SelectMove.L_NextQuestionCard.Visible := false;
  F_SelectDiscipline.Close;
  end;

end;

end.
