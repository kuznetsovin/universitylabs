object F_SelectFunctionalSubsystem: TF_SelectFunctionalSubsystem
  Left = 0
  Top = 0
  Caption = #1042#1099#1073#1086#1088' '#1087#1086#1076#1089#1080#1089#1090#1077#1084#1099
  ClientHeight = 285
  ClientWidth = 565
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object L_Teaching: TLabel
    Left = 144
    Top = 72
    Width = 139
    Height = 33
    Alignment = taCenter
    Caption = #1054#1073#1091#1095#1077#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = L_TeachingClick
    OnMouseMove = L_TeachingMouseMove
    OnMouseLeave = L_TeachingMouseLeave
  end
  object L_KnowledgeControl: TLabel
    Left = 144
    Top = 135
    Width = 246
    Height = 33
    Alignment = taCenter
    Caption = #1050#1086#1085#1090#1088#1086#1083#1100' '#1079#1085#1072#1085#1080#1081
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = KnowledgeClick
    OnMouseMove = L_KnowledgeControlMouseMove
    OnMouseLeave = L_KnowledgeControlMouseLeave
  end
end
