{����� ������ �������������� ����������. � ��� ���������� �����
������ ������ ���� ��������, ���� ������������}
unit SelectFunctionalSubsystem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Global;

type
  TF_SelectFunctionalSubsystem = class(TForm)
    L_Teaching: TLabel;
    L_KnowledgeControl: TLabel;
    procedure L_TeachingMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure L_TeachingMouseLeave(Sender: TObject);
    procedure L_KnowledgeControlMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure L_KnowledgeControlMouseLeave(Sender: TObject);
    procedure KnowledgeClick(Sender: TObject);
    procedure L_TeachingClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_SelectFunctionalSubsystem: TF_SelectFunctionalSubsystem;

implementation

uses SelectDiscipline;



{$R *.dfm}


procedure TF_SelectFunctionalSubsystem.L_KnowledgeControlMouseLeave(
  Sender: TObject);
begin
//��� �������� ������� � ������
HTMLLinkOff (L_KnowledgeControl);
end;

procedure TF_SelectFunctionalSubsystem.L_KnowledgeControlMouseMove(
  Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
//��� ��������� ������� �� ������
HTMLLinkOn (L_KnowledgeControl);
end;

procedure TF_SelectFunctionalSubsystem.FormCreate(Sender: TObject);
begin
//������������� ���������� ������
bg_SelectRate := True;
end;

procedure TF_SelectFunctionalSubsystem.KnowledgeClick(Sender: TObject);
begin
//----�������----------
//Application.CreateForm(TF_SelectDiscipline, F_SelectDiscipline);

//��������� � ��������� �����
F_SelectDiscipline.Show;
//��������� ���� �������������� ����������
F_SelectFunctionalSubsystem.Close;
//�������� ����� �������� ������
bg_SelectRate := True;
end;

procedure TF_SelectFunctionalSubsystem.L_TeachingClick(Sender: TObject);
begin
//----�������----------
//Application.CreateForm(TF_SelectDiscipline, F_SelectDiscipline);

//��������� � ��������� �����
F_SelectDiscipline.Show;
//��������� ���� �������������� ����������
F_SelectFunctionalSubsystem.Close;
//�������� ����� ��������
bg_SelectRate := False;
end;


procedure TF_SelectFunctionalSubsystem.L_TeachingMouseLeave(Sender: TObject);
begin
//��� �������� ������� � ������
HTMLLinkOff (L_Teaching);
end;

procedure TF_SelectFunctionalSubsystem.L_TeachingMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
//��� ��������� ������� �� ������
HTMLLinkOn (L_Teaching);
end;

end.
