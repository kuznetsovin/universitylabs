object F_SelectMove: TF_SelectMove
  Left = 0
  Top = 0
  ClientHeight = 169
  ClientWidth = 476
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Arial'
  Font.Style = [fsBold]
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 19
  object L_NextQuestionCard: TLabel
    Left = 32
    Top = 8
    Width = 221
    Height = 19
    Caption = #1042#1099#1073#1086#1088' '#1089#1083#1077#1076#1091#1102#1097#1077#1075#1086' '#1073#1080#1083#1077#1090#1072
    OnClick = L_NextQuestionCardClick
    OnMouseMove = L_NextQuestionCardMouseMove
    OnMouseLeave = L_NextQuestionCardMouseLeave
  end
  object L_Login: TLabel
    Left = 32
    Top = 40
    Width = 414
    Height = 19
    Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100' '#1080#1076#1077#1085#1090#1080#1092#1080#1082#1072#1094#1080#1102' '#1076#1088#1091#1075#1086#1075#1086' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103
    OnClick = L_LoginClick
    OnMouseMove = L_LoginMouseMove
    OnMouseLeave = L_LoginMouseLeave
  end
  object L_SelectSystemTeaching: TLabel
    Left = 32
    Top = 79
    Width = 304
    Height = 19
    Caption = #1042#1099#1073#1086#1088' '#1092#1091#1085#1082#1094#1080#1086#1085#1072#1083#1100#1085#1086#1081' '#1087#1086#1076#1089#1080#1089#1090#1077#1084#1099
    OnClick = L_SelectSystemTeachingClick
    OnMouseMove = L_SelectSystemTeachingMouseMove
    OnMouseLeave = L_SelectSystemTeachingMouseLeave
  end
  object L_Exit: TLabel
    Left = 32
    Top = 112
    Width = 55
    Height = 19
    Caption = #1042#1099#1093#1086#1076
    OnClick = L_ExitClick
    OnMouseMove = L_ExitMouseMove
    OnMouseLeave = L_ExitMouseLeave
  end
end
