unit SelectMove;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Global, StdCtrls;

type
  TF_SelectMove = class(TForm)
    L_NextQuestionCard: TLabel;
    L_Login: TLabel;
    L_SelectSystemTeaching: TLabel;
    L_Exit: TLabel;
    procedure L_SelectSystemTeachingClick(Sender: TObject);
    procedure L_LoginClick(Sender: TObject);
    procedure L_NextQuestionCardClick(Sender: TObject);
    procedure L_ExitClick(Sender: TObject);
    procedure L_NextQuestionCardMouseLeave(Sender: TObject);
    procedure L_NextQuestionCardMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure L_LoginMouseLeave(Sender: TObject);
    procedure L_LoginMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure L_SelectSystemTeachingMouseLeave(Sender: TObject);
    procedure L_SelectSystemTeachingMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure L_ExitMouseLeave(Sender: TObject);
    procedure L_ExitMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_SelectMove: TF_SelectMove;

implementation

uses QuestionList, Login, SelectFunctionalSubsystem, Enter;

{$R *.dfm}

procedure TF_SelectMove.L_NextQuestionCardClick(Sender: TObject);
begin
F_QuestionList.Show;
Close;
end;

procedure TF_SelectMove.L_NextQuestionCardMouseLeave(Sender: TObject);
begin
HTMLLinkOff(L_NextQuestionCard);
end;

procedure TF_SelectMove.L_NextQuestionCardMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
HTMLLinkOn(L_NextQuestionCard);
end;

procedure TF_SelectMove.L_LoginClick(Sender: TObject);
begin
F_Login.Show;
Close;
end;

procedure TF_SelectMove.L_LoginMouseLeave(Sender: TObject);
begin
HTMLLinkOff(L_Login);
end;

procedure TF_SelectMove.L_LoginMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
HTMLLinkOn(L_Login);
end;

procedure TF_SelectMove.L_SelectSystemTeachingClick(Sender: TObject);
begin
F_SelectFunctionalSubsystem.Show;
Close;
end;

procedure TF_SelectMove.L_SelectSystemTeachingMouseLeave(Sender: TObject);
begin
HTMLLinkOff(L_SelectSystemTeaching);
end;

procedure TF_SelectMove.L_SelectSystemTeachingMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
HTMLLinkOn(L_SelectSystemTeaching);
end;

procedure TF_SelectMove.FormShow(Sender: TObject);
begin
      F_SelectMove.L_Login.Visible := true;
      F_SelectMove.L_NextQuestionCard.Visible := true;
end;

procedure TF_SelectMove.L_ExitClick(Sender: TObject);
begin
F_Enter.Close;
Close;
end;

procedure TF_SelectMove.L_ExitMouseLeave(Sender: TObject);
begin
HTMLLinkOff(L_Exit);
end;

procedure TF_SelectMove.L_ExitMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
HTMLLinkOn(L_Exit);
end;

end.
