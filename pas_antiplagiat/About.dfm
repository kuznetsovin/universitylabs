object AboutBox1: TAboutBox1
  Left = 200
  Top = 108
  BorderStyle = bsDialog
  Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077
  ClientHeight = 250
  ClientWidth = 363
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 8
    Width = 353
    Height = 201
    BevelInner = bvRaised
    BevelOuter = bvLowered
    ParentColor = True
    TabOrder = 0
    object Copyright: TLabel
      Left = 16
      Top = 80
      Width = 44
      Height = 13
      Caption = 'Copyright'
      IsControl = True
    end
    object Label2: TLabel
      Left = 16
      Top = 56
      Width = 61
      Height = 13
      Caption = #1042#1077#1088#1089#1080#1103' 1.2b'
    end
    object Label3: TLabel
      Left = 64
      Top = 80
      Width = 222
      Height = 13
      Caption = #1048#1075#1086#1088#1100' '#1050#1091#1079#1085#1077#1094#1086#1074' (sk0tch) '#1048#1050#1059' '#1056#1043#1040#1047#1059' 2007'#1075'.'
    end
    object Label4: TLabel
      Left = 16
      Top = 104
      Width = 321
      Height = 89
      AutoSize = False
      Caption = 
        #1044#1072#1085#1085#1072#1103' '#1087#1088#1086#1075#1088#1072#1084#1072' '#1088#1072#1089#1087#1088#1086#1089#1090#1072#1088#1103#1077#1090#1089' '#1087#1086' '#1083#1080#1094#1077#1085#1079#1080#1080' GNU '#1074#1077#1088#1089#1080#1080' 2 (http://' +
        'www.gnu.org) '#1080' '#1085#1077' '#1087#1088#1080#1076#1077#1085#1076#1091#1077#1090' '#1085#1072' '#1085#1072#1079#1074#1072#1085#1080#1077' '#1087#1086#1076#1086#1073#1085#1099#1093' '#1087#1088#1086#1075#1088#1072#1084#1084'. '#1055#1083#1072#1090 +
        #1072' '#1079#1072' '#1080#1089#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1077' '#1087#1088#1086#1075#1088#1072#1084#1084#1099' '#1084#1086#1078#1077#1090' '#1074#1079#1080#1084#1072#1090#1100#1089#1103' '#1090#1086#1083#1100#1082#1086' '#1072#1074#1090#1086#1088#1086#1084', '#1083#1080#1073 +
        #1086' '#1074#1099#1096#1077#1086#1087#1080#1089#1072#1085#1085#1086#1081' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1077#1081'. '#1040#1074#1090#1086#1088' '#1085#1077' '#1085#1077#1089#1077#1090' '#1086#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1086#1089#1090#1080' '#1079#1072' ' +
        #1085#1077#1080#1089#1087#1088#1072#1074#1085#1086#1089#1090#1080' '#1080' '#1085#1077#1090#1086#1095#1085#1086#1089#1090#1080' '#1074' '#1076#1072#1085#1085#1086#1081' '#1087#1088#1086#1075#1088#1072#1084#1084#1077'. '#1058'.'#1082'.  '#1086#1085#1072' '#1087#1080#1089#1072#1083#1072#1089 +
        #1100' '#1076#1083#1103' '#1083#1080#1095#1085#1086#1075#1086' '#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1077#1103'!'
      Transparent = True
      WordWrap = True
    end
    object Label1: TLabel
      Left = 96
      Top = 24
      Width = 127
      Height = 19
      Caption = #1040#1053#1058#1048#1055#1051#1040#1043#1048#1040#1058' '
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object OKButton: TButton
    Left = 143
    Top = 220
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = OKButtonClick
  end
end
