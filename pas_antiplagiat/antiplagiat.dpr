program antiplagiat;

uses
  Forms,
  main in 'main.pas' {Form1},
  dobavlenie in 'dobavlenie.pas' {Form2},
  proverka in 'proverka.pas' {Form3},
  About in 'About.pas' {AboutBox1};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm3, Form3);
  Application.CreateForm(TAboutBox1, AboutBox1);
  Application.Run;
end.
