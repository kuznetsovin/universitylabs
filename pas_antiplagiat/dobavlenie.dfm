object Form2: TForm2
  Left = 309
  Top = 114
  Width = 315
  Height = 209
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1074' '#1073#1072#1079#1091
  Color = clBtnFace
  Constraints.MaxHeight = 209
  Constraints.MaxWidth = 315
  Constraints.MinHeight = 209
  Constraints.MinWidth = 315
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'System'
  Font.Style = [fsBold]
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 16
  object Label2: TLabel
    Left = 16
    Top = 56
    Width = 136
    Height = 16
    Caption = #1059#1082#1072#1078#1080#1090#1077' '#1087#1091#1090#1100' '#1082' '#1073#1072#1079#1077
  end
  object OpenBut: TButton
    Left = 16
    Top = 16
    Width = 153
    Height = 33
    Caption = #1054#1090#1082#1088#1099#1090#1100' '#1092#1072#1081#1083' Word'
    TabOrder = 0
    OnClick = OpenButClick
  end
  object Edit1: TEdit
    Left = 16
    Top = 80
    Width = 241
    Height = 24
    TabOrder = 1
  end
  object PasteBut: TButton
    Left = 24
    Top = 136
    Width = 129
    Height = 33
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074' '#1073#1072#1079#1091
    TabOrder = 2
    OnClick = PasteButClick
  end
  object ProgressBar: TProgressBar
    Left = 16
    Top = 112
    Width = 281
    Height = 16
    TabOrder = 3
  end
  object Button1: TButton
    Left = 264
    Top = 80
    Width = 33
    Height = 25
    Caption = '...'
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 160
    Top = 136
    Width = 129
    Height = 33
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 5
    OnClick = Button2Click
  end
  object WordDocBase: TWordDocument
    AutoConnect = False
    ConnectKind = ckRunningOrNew
    Left = 184
    Top = 16
  end
  object WordAppBase: TWordApplication
    AutoConnect = False
    ConnectKind = ckRunningOrNew
    AutoQuit = False
    Left = 216
    Top = 16
  end
  object OpenBase: TOpenDialog
    Filter = #1060#1072#1081#1083#1099' Word|*.doc|'#1060#1072#1081#1083' '#1073#1072#1079#1099'|*.baza'
    Left = 248
    Top = 16
  end
  object SaveBase: TSaveDialog
    Filter = #1060#1072#1081#1083' '#1073#1072#1079#1099'|*.baza'
    Left = 248
    Top = 48
  end
end
