unit dobavlenie;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, WordXP, OleServer, Word2000, ComCtrls;

type
  TForm2 = class(TForm)
    Edit1: TEdit;
    PasteBut: TButton;
    WordDocBase: TWordDocument;
    WordAppBase: TWordApplication;
    OpenBase: TOpenDialog;
    OpenBut: TButton;
    ProgressBar: TProgressBar;
    Label2: TLabel;
    Button1: TButton;
    SaveBase: TSaveDialog;
    Button2: TButton;
    procedure OpenButClick(Sender: TObject);
    procedure PasteButClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;
  temp: WideString;

implementation

uses main;

{$R *.dfm}
procedure dobavit_v_bazu;
var file_bazi: TextFile;
    predlozhenie: string;
    sym: longint;
begin
sym := 1;
  Form2.ProgressBar.Step := Round(length(temp)/100);
  //������ �������� ����������
  AssignFile(file_bazi, Form2.Edit1.Text);
  {$I-}
  //��������� �� ����������
  Append(file_bazi);
  //���� ����� ���, �� �������
  if ioresult <> 0 then Rewrite(file_bazi);
  {$I+}
  //���� � ������ ���� �������
  while sym <= length(temp) do begin
    //���� ����������� �� �����������
    while (temp[sym] <> '.') and (temp[sym] <> '?') and
      (temp[sym] <> '!') and (sym <= length(temp)) do begin
        //���������� ������� �������� � �����������
        predlozhenie := predlozhenie + temp[sym];
        //��������� � ���������� �������
        sym := sym + 1;
        end;
    //���������� ����������� � ����
    write(file_bazi, predlozhenie + '|');
    //��������� � ���������� �������
    sym := sym + 1;
    predlozhenie := '';
    Form2.ProgressBar.StepIt;
    end;
  //��������� ����
  CloseFile(file_bazi);
  ExitThread(0);
end;

procedure TForm2.OpenButClick(Sender: TObject);
var dir: OleVariant;
begin
  //���������� ���� ��������
  OpenBase.Execute;
  //���� ���� ��� �����
  if OpenBase.FileName <> '' then begin
    //���������� ����
    dir := OpenBase.FileName;
    //��������� �������� Word
    WordAppBase.Connect;
    WordAppBase.Documents.Open(dir,EmptyParam,EmptyParam,
                                   EmptyParam,EmptyParam,EmptyParam,EmptyParam,
                                   EmptyParam,EmptyParam,EmptyParam,EmptyParam,
                                   EmptyParam);
    WordDocBase.ConnectTo(WordAppBase.ActiveDocument);
    //����� "����"
    end;
    //���������� ����� �� �����
    temp := WordDocBase.Range.Text;
end;

procedure TForm2.PasteButClick(Sender: TObject);
var th2:cardinal;
begin
  //���� ���� �����
  if Edit1.Text <> '' then begin
    //������� ����� ��� ���������� ������
    CreateThread(nil,1024,@dobavit_v_bazu,self,0,th2);
    //������� ������
    ShowMessage('����� ������� �������� � ����');
    //��������� ��������
    WordDocBase.Close;
    //����� "����"
    end
  //���� ����� ���
  else begin
    ShowMessage('�� ����� ���� ��� �������� ����');
    //����� "����"
    end;
end;

procedure TForm2.Button1Click(Sender: TObject);
begin
  //���������� ������������ � ��������
  if MessageDlg('������� ����� ����?', mtConfirmation,
    [mbYes, mbNo], 0) = mrYes then begin
      //���������� ���� ����������
      SaveBase.Execute;
      //���� ���� ��� ���������� ������
      if SaveBase.FileName <> '' then begin
        //���������� ����
        Edit1.Text := SaveBase.FileName + '.baza';
        //����� "����"
        end;
      //����� "����"
      end
  else begin
    //��������� ���� �������
    OpenBase.Execute;
    //���� ���� ������
    if OpenBase.FileName <> '' then begin
      //���������� ���� � ����
      Edit1.Text := OpenBase.FileName;
      //����� "����"
      end;
    //����� "����"
    end;
end;

procedure TForm2.Button2Click(Sender: TObject);
begin
  //���� �������������� ���������� ����������
  if MessageDlg('���� �� �������� �����, �� ������� ���������� (���� �� ����) ������������ � ������� ������',
    mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
      //������� ����� ����������
      Form2.Close;
      //����� "����"
      end;
end;

end.
