object Form3: TForm3
  Left = 628
  Top = 205
  Width = 339
  Height = 212
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1085#1072' '#1087#1083#1072#1075#1080#1072#1090
  Color = clBtnFace
  Constraints.MaxHeight = 212
  Constraints.MaxWidth = 339
  Constraints.MinHeight = 212
  Constraints.MinWidth = 339
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'System'
  Font.Style = [fsBold]
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 136
    Height = 16
    Caption = #1059#1082#1072#1078#1080#1090#1077' '#1087#1091#1090#1100' '#1082' '#1073#1072#1079#1077
  end
  object OpenDocBut: TButton
    Left = 16
    Top = 64
    Width = 153
    Height = 33
    Caption = #1060#1072#1081#1083' '#1076#1083#1103' '#1087#1088#1086#1074#1077#1088#1082#1080
    TabOrder = 0
    OnClick = OpenDocButClick
  end
  object Edit1: TEdit
    Left = 16
    Top = 32
    Width = 257
    Height = 24
    TabOrder = 1
  end
  object DoingBut: TButton
    Left = 16
    Top = 136
    Width = 145
    Height = 33
    Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100' '#1087#1088#1086#1074#1077#1088#1082#1091
    TabOrder = 2
    OnClick = DoingButClick
  end
  object ProgressBar: TProgressBar
    Left = 16
    Top = 112
    Width = 297
    Height = 16
    TabOrder = 3
  end
  object Button1: TButton
    Left = 280
    Top = 32
    Width = 33
    Height = 25
    Caption = '...'
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 168
    Top = 136
    Width = 145
    Height = 33
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 5
    OnClick = Button2Click
  end
  object WordDocFile: TWordDocument
    AutoConnect = False
    ConnectKind = ckRunningOrNew
    Left = 192
    Top = 64
  end
  object WordAppFile: TWordApplication
    AutoConnect = False
    ConnectKind = ckRunningOrNew
    AutoQuit = False
    Left = 224
    Top = 64
  end
  object OpenFile: TOpenDialog
    Filter = #1060#1072#1081#1083#1099' '#1073#1072#1079#1099'|*.baza|'#1044#1086#1082#1091#1084#1077#1085#1090' Word|*.doc'
    Left = 256
    Top = 64
  end
end
