unit proverka;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, WordXP, OleServer, StdCtrls, Word2000, ComCtrls;

type
  TForm3 = class(TForm)
    OpenDocBut: TButton;
    Edit1: TEdit;
    DoingBut: TButton;
    WordDocFile: TWordDocument;
    WordAppFile: TWordApplication;
    OpenFile: TOpenDialog;
    ProgressBar: TProgressBar;
    Label1: TLabel;
    Button1: TButton;
    Button2: TButton;
    procedure OpenDocButClick(Sender: TObject);
    procedure DoingButClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;
  temp: WideString;
  vsego_predlozheniy, shodstvo: longint;

implementation

{$R *.dfm}
procedure chet_procenta_povtoreniy(var shozh_predlozheniya, obshee_chislo: longint);
var resultat: real;
begin
   resultat := obshee_chislo;
  //���� ����������� ����
  if obshee_chislo <> 0 then begin
    //������� ���������
    resultat := shozh_predlozheniya/obshee_chislo*100
    end;
  ShowMessage('������� �������� � ������ ������: ' +
  FloatToStr(resultat));
end;

procedure otkrit_bazu;
begin
  //��������� ���� �������
  Form3.OpenFile.Execute;
  //���� ���� ��� �����
  if Form3.OpenFile.FileName <> '' then begin
    //���������� ����
    Form3.Edit1.Text := Form3.OpenFile.FileName;
    //����� "����"
    end;
end;

procedure proverit_rabotu;
var file_proverki: TextFile;
    net_predlozheniya: boolean;
    buf,predlozhenie: string;
    simvol_iz_bazi: char;
    sym : longint;

begin
  net_predlozheniya := false;
  sym := 1;
  //������ �������� ����������
  AssignFile(file_proverki, Form3.Edit1.Text);
  Form3.ProgressBar.Step := Round(length(temp)/100);
  //���� ����������� ����� �� ����������
  while sym <= length(temp) do begin
   //���� ����������� �� �����������
   while (temp[sym] <> '.') and (temp[sym] <> '?') and
    (temp[sym] <> '!') and (sym <= length(temp)) do begin
      //���������� ������� �������� � �����������
      predlozhenie := predlozhenie + temp[sym];
      //��������� � ���������� �������
      sym := sym + 1;
      //����� "����"
      end;
    //����� ����� �����������
    vsego_predlozheniy := vsego_predlozheniy + 1;
    //��������� ���� ���� �� ������
    Reset(file_proverki);
    //���� �� ����� ����
    while not EOF(file_proverki) do begin
      //��������� ������
      read(file_proverki, simvol_iz_bazi);
      //���� ��� �� ����� ����������
      if simvol_iz_bazi <> '|' then begin
        //�������� ������� � �����
        buf := buf + simvol_iz_bazi;
        //����� "����"
        end
      else begin
        //���� ����� ����������� ���� � ����
        if buf = predlozhenie then begin
          //�������� ����������� �� 1
          shodstvo := shodstvo + 1;
          //������ ����� � ����������
           net_predlozheniya := true;
          //����� "����"
          end;
        //�������� �����
        buf := '';
        //����� "����"
        end;
      //����� "����"
      end;
  if not net_predlozheniya then begin
    CloseFile(file_proverki);
    Append(file_proverki);
    write(file_proverki,predlozhenie + '|');
    CloseFile(file_proverki);
    end;
  sym := sym + 1;
  //�������� �����������
  predlozhenie := '';
  Form3.ProgressBar.StepIt;
  //����� "����"
  end;
  //���� ����������� ����
  chet_procenta_povtoreniy(shodstvo, vsego_predlozheniy);
  //��������� �����
  ExitThread(0);
end;
procedure TForm3.OpenDocButClick(Sender: TObject);
var dir: OleVariant;
begin
  //���������� ���� ��������
  OpenFile.Execute;
  //���� ���� ��� �����
  if OpenFile.FileName <> '' then begin
    //���������� ����
    dir := OpenFile.FileName;
    //��������� �������� Word
    WordAppFile.Connect;
    WordAppFile.Documents.Open(dir,EmptyParam,EmptyParam,
                                   EmptyParam,EmptyParam,EmptyParam,EmptyParam,
                                   EmptyParam,EmptyParam,EmptyParam,EmptyParam,
                                   EmptyParam);
    WordDocFile.ConnectTo(WordAppFile.ActiveDocument);
    //����� "����"
    end;
    //���������� ����� � �����
    temp := Form3.WordDocFile.Range.Text;
end;

procedure TForm3.DoingButClick(Sender: TObject);
var th1:cardinal;
begin
  //���� ���� � ���� ����
  if Form3.Edit1.Text <> '' then begin
    //������� ����� ��� �������� ������;
    CreateThread(nil,1024,@proverit_rabotu,self,0,th1);
    //��������� ����
    WordDocFile.Close;
    //����� "����"
    end
  //���� ���� �� ���� ���
  else begin
     ShowMessage('�������� ���� ��������!');
     otkrit_bazu;
    //����� "����"
    end;
end;

procedure TForm3.Button1Click(Sender: TObject);
begin
{  //��������� ���� �������
  OpenFile.Execute;
  //���� ���� ��� �����
  if OpenFile.FileName <> '' then begin
    //���������� ����
    Edit1.Text := OpenFile.FileName;
    //����� "����"
    end;    }
  otkrit_bazu;
end;

procedure TForm3.Button2Click(Sender: TObject);
begin
  //����������� �������� ������������
  if MessageDlg('�� ������� ��� ������ �������� ������� ��������?',
    mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
      chet_procenta_povtoreniy(shodstvo, vsego_predlozheniy);
      //������� ����� ��������
      Form3.Close;
      //����� "����"
      end;
end;

procedure TForm3.FormCreate(Sender: TObject);
begin
  shodstvo := 0;
  vsego_predlozheniy := 0;
  end;

end.
